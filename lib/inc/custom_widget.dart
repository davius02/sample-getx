import 'package:flutter/material.dart';

class CardGradient extends StatelessWidget {
  final List<Color>? colors;
  final double? radius;
  final double? height;
  final double? width;
  const CardGradient({super.key, this.colors,this.radius,this.height,this.width});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: colors==null?[Color(0xFF474C72), Color(0xFF90B3D3)]:colors!,
          stops: [0, 1],
          begin: AlignmentDirectional(1, 0.64),
          end: AlignmentDirectional(-1, -0.64),
        ),
        borderRadius: BorderRadius.circular(radius==null?30:radius!),
      ),
    )
    ;
  }
}