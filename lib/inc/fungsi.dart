
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sample/inc/config.dart';

Future get_data() async {
  final response = await http.get(Uri.parse('$api_url/users/hadley/orgs'));
  return jsonDecode(response.body);
}