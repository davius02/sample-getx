import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:sample/controller/HomeController.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  // Memanggil Home Controller jika belum di destroy
  final HomeController _cHome = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Obx(()=>Text('Profile '+_cHome.nama.value)),
      ),
      body: ListView(
        children: List.generate(_cHome.list_data.value.length, (i) => Card(
          child: ListTile(
            title: Text(_cHome.list_data.value[i]['repos_url'].toString()),
            leading: CircleAvatar(backgroundImage: NetworkImage(_cHome.list_data.value[i]['avatar_url'].toString()),),
          ),
        )),
      ),
    );
  }
}