import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:sample/controller/HomeController.dart';
import 'package:sample/inc/fungsi.dart';
import 'package:sample/page/profile.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // Memanggil Home Controller jika belum di destroy
  final HomeController _cHome = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sample Lesson'),
      ),
      body: Obx(()=>ListView(
        padding: EdgeInsets.all(10),
        children: [
          ElevatedButton(
            onPressed: ()async {
              var result = await get_data();
              _cHome.list_data.value = result;
              _cHome.nama.value = 'Jimmy Andrian Davius';
              _cHome.umur.value = 25;
            },
            child: Text('Get Data'),
          ),
          SizedBox(height: 10,),
          ElevatedButton(
            onPressed: _cHome.list_data.length==0?null:() {
              // Buka Page
              Get.to(ProfilePage());
            },
            child: Text('Open Profile'),
          ),
        ],
      )),
    );
  }
}