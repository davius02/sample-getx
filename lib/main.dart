import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sample/controller/HomeController.dart';
import 'package:sample/inc/config.dart';
import 'package:sample/page/home.dart';

void main() {
  // Memanggil Controller di awal
  final HomeController _cHome = Get.put(HomeController());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  // Gunakan GetMaterialApp
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: '$title_apps',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}
